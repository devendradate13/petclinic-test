FROM maven
WORKDIR /app
COPY . /app
RUN mvn dependency:go-offline
RUN mvn clean package -DskipTests

FROM openjdk:8-jre-alpine
COPY --from=0 /app/target/*.jar .
CMD java -jar *.jar
EXPOSE 8080
